package Tute2;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;


public class BasicSwing extends JFrame {

	JPanel p=new JPanel();
	JButton b = new JButton("Hello!");
	JTextField t = new JTextField("Hi", 20);
	JTextArea ta = new JTextArea("How\nare\nyou?", 5, 20);
	JLabel l = new JLabel("What's up?");
	String choices[] = {
		"Hallo",
		"Bonjour",
		"Conichuva"
	};
	JComboBox cb = new JComboBox(choices);
	
	public static void main(String[] args) {
		new BasicSwing();

	}
	
	public BasicSwing() {
		super("Basic Swing App");
		setSize(400,300);
		setResizable(true);
		
		// first we'll add things to p
		p.add(b);
		p.add(t);
		p.add(ta);
		p.add(l);
		p.add(cb);
		
		// then add p to frame
		add(p);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

}
